# Use go 1.x based on the latest alpine image.
FROM golang:1.18.5-alpine AS build

# Disable go-modules
ENV GO111MODULE=off

# Install build tools.
RUN apk add --update make git

ADD . app/
WORKDIR app/
RUN make install

# Release binary on latest alpine image.
FROM alpine:latest
COPY --from=build /go/bin/sally /usr/local/bin/sally
ADD . app/
WORKDIR app/

EXPOSE 8080
CMD ["/usr/local/bin/sally" , "-yml", "sally.yaml", "-port", "8080"]